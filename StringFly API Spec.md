
# StringFly API Spec

***
  
### Login  
  
This API call is to authorize a user with username and password. It will return an OAuth token that is used to authorize the application for all future sessions.  

**End Point:** https://api.stringfly.com/v2/functions/oath2/login  
**Request Method:** POST  
**Headers:** X-API-KEY  
**Parameters:**  
> 
> username
> :  The username for the user using the application (required)
> 
> password
> :  The password for the user of the application (required)
> 
> grant_type
> :  Type of oauth grant (always "password") (required)
> 
> app_uid
> :  a unique identifier for this installation of the application (required)
> 
> app_description
> : a text description for this device or service (examples: "John's iPhone" and "My Cool Webservice") (required)  

**Response**:  
  

JSON Success:  

     {
          "success": 1,
          "error": {
        },
       "content": {
          "refresh_token": "323f58f5906aad8",
          "token_type": "normal",
          "access_token": "MnxjMTVlMjUzZjYzNzcxODY5ZTIwODk2MjhlOTJhNjEwMw,,",
          "expires_in": null
      }
  }

JSON Failure:

	{
		"success": 0,
		"error": {
                 "code": 123, 
                 "message": "sample error message"
                 }
			
	}

	
XML Success:

    <?xml version="1.0" encoding="utf-8"?>
    <xml>
        <success>1</success>
        <content>
            <access_token>wDfUjbIHu4Q...</access_token>
        </content>
    </xml>

XML Failure:  

    <?xml version="1.0" encoding="utf-8"?>
    <xml>
        <success>0</success>
        <error>
                <code>123</code>
                <message>sample error message</message>
        </error>  
    </xml>

***

### List User Assignments 
**End Point:** https://api.stringfly.com/v2/user/{user_id}/assignments  
**Request Method:** GET  
**Parameters:** None  
**Headers:** X-API-KEY, ACCESS-TOKEN  

**JSON Success**
  
	{
    "success": 1,
    "content": [
        {
            "accepted": "1",
            "logo_url": "http://developmedia.sweneno.com/profile_pics/large/17331429934ed4f94b0d7455.88755667_2089942535.jpg",
            "name": "chobi6",
            "id": "1398"
        }
    ]
	}

***

### Get User Detail 
**End Point:** https://api.stringfly.com/v2/user/{user_id}  
**Request Method:** GET  
**Parameters:** None  
**Headers:** X-API-KEY, ACCESS-TOKEN  
  
**JSON Success:**

	{
    "success": 1,
    "content": [
        {
            "picsmall_url": "",
            "sms_verified": "0",
            "language": "0",
            "mbp_state": "",
            "cell_phone": "781 910 0468",
            "cc_exp_dte": "0000-00-00",
            "home_longitude": "Unknown",
            "password": "5f4dcc3b5aa765d61d8327deb882cf99",
            "work_history": "",
            "last_pay_date": "0000-00-00",
            "registerip": "",
            "mbp_last_name": "",
            "address1": "",
            "profit_share": "0",
            "proxied_email": "",
            "address2": "",
            "custom_recording_location": null,
            "assignment_max_reporters": "25",
            "photobucket_public": "0",
            "logo_url": "",
            "current_altitude": "",
            "systemlang": "1",
            "debit_card_first_name": "",
            "current_datetime": "2012-02-24 19:09:25",
            "country": "0",
            "phone": "",
            "email_address": "makrism@comcast.net",
            "mbp_address1": "",
            "allow_contests_nots": "0",
            "mb_net_type": "0",
            "user_name": "makrism",
            "last_receipt_date": "0000-00-00",
            "age": "0",
            "a2billing_account_no": null,
            "debit_card_last_name": "",
            "time_zone": "0",
            "mbp_country": "",
            "last_name": "makris",
            "quality_ranking": "",
            "first_name": "milton",
            "ar_balance": "0.00",
            "bio": "",
            "sms_use_carriers": "0",
            "zip": "",
            "fin_bill_addr_check": "0",
            "currency_symbol": "$",
            "allow_contacts": "0",
            "sms_retries": "0",
            "ssn": "",
            "birth_range": null,
            "entrypoint": null,
            "social_id": null,
            "picbig_url": "",
            "flag_review_user": "0",
            "secret_token": null,
            "education": "",
            "exteno": "",
            "pay_terms": "",
            "flagged_by": "0",
            "logged_out_on": null,
            "salutation": "0",
            "debit_card_cvv": "",
            "city": "",
            "mbp_zip": "",
            "wireless_email": "",
            "systemnots": "01001",
            "unique_id": "9694536284d2cb6127352e4.25551373",
            "profile_updated": "0000-00-00 00:00:00",
            "debit_card_type": "",
            "androidurl": null,
            "flag_date": "0000-00-00 00:00:00",
            "accept_assignments": "0",
            "mbp_address2": "",
            "last_post_date": "0000-00-00",
            "mbp_city": "",
            "flikr_public": "0",
            "sms_address": "",
            "stringfly": "0",
            "thirdparty_assignments": "0",
            "cc_type": "",
            "education_level": null,
            "cc_first_name": "",
            "mbp_phone": "",
            "about_me": "",
            "mbp_extno": "",
            "occupation": null,
            "state": "",
            "itunesurl": null,
            "activation_string": "",
            "mbp_email_address": "",
            "flag_comment": "",
            "register_date": "2011-01-11 14:57:06",
            "user_type": "Author",
            "cc_no": "",
            "current_longitude": "-71.188083",
            "flagged": "0",
            "fin_contact_check": "0",
            "mbp_first_name": "",
            "allow_gps_tracking": "0",
            "wireless_verified": "0",
            "company": "",
            "user_permissions": "00000",
            "link": "",
            "home_latitude": "Unknown",
            "anonymous": "0",
            "organization_type": "",
            "status": "1",
            "bburl": null,
            "allow_custom_recordings": "0",
            "numeric_ranking": null,
            "profile_id": "0",
            "last_visit_date": "0000-00-00 00:00:00",
            "payment_days": "0",
            "auth_type": null,
            "current_latitude": "42.524656",
            "allow_sms": "0",
            "cell_carrier": "",
            "paypal_acc_no": "",
            "reason": "",
            "flag_review_date": "0000-00-00 00:00:00",
            "locale": "0",
            "user_specialty": "",
            "create_assignments": "1",
            "last_sale_date": "0000-00-00",
            "ap_balance": "0.00",
            "gender": "",
            "cc_last_name": "",
            "verified": "0",
            "debit_card_no": "",
            "id": "754",
            "cc_cvv": "",
            "birthday": "0000-00-00",
            "dcard_expdte": "0000-00-00",
            "picture_url": "",
            "master_user_id": "0"
        }
    ]
	}

***

### Get User Detail For Logged In User (the owner of the OAuth2 Token)
**End Point:** https://api.stringfly.com/v2/user  
**Request Method:** GET  
**Parameters:** None  
**Headers:** X-API-KEY, ACCESS-TOKEN  
  
**JSON Success:**  
  
	{
    "success": 1,
    "content": {
        "picsmall_url": null,
        "sms_verified": "0",
        "language": "0",
        "mbp_state": "",
        "cell_phone": "",
        "cc_exp_dte": "2010-01-00",
        "home_longitude": "-84.045059",
        "password": "6f3a7dfd21aa104ad7b34efd6163c6f5",
        "work_history": null,
        "last_pay_date": null,
        "registerip": "",
        "mbp_last_name": "",
        "address1": "San Pedro",
        "profit_share": "0",
        "proxied_email": "",
        "address2": "",
        "custom_recording_location": null,
        "assignment_max_reporters": "25",
        "photobucket_public": "0",
        "logo_url": null,
        "current_altitude": "",
        "systemlang": "1",
        "debit_card_first_name": "",
        "current_datetime": null,
        "country": "190",
        "phone": "50688618075",
        "email_address": "marthaiglesias88@sweneno.com",
        "mbp_address1": "",
        "allow_contests_nots": "0",
        "mb_net_type": "0",
        "user_name": "marthamb",
        "last_receipt_date": null,
        "age": "0",
        "a2billing_account_no": "1225081348",
        "debit_card_last_name": "",
        "time_zone": "0",
        "mbp_country": "",
        "last_name": "Iglesias",
        "quality_ranking": "",
        "first_name": "Martha",
        "ar_balance": "0.00",
        "bio": "",
        "sms_use_carriers": "0",
        "zip": "45678",
        "fin_bill_addr_check": "0",
        "currency_symbol": "",
        "allow_contacts": "0",
        "sms_retries": "0",
        "ssn": "",
        "birth_range": null,
        "entrypoint": null,
        "social_id": null,
        "picbig_url": null,
        "flag_review_user": "0",
        "secret_token": "e462debfdf4f81881f7bf1776fafdb5a",
        "education": null,
        "exteno": "",
        "pay_terms": "",
        "flagged_by": "0",
        "logged_out_on": "2012-12-04 12:45:02",
        "salutation": "",
        "debit_card_cvv": "",
        "city": "San Jose",
        "mbp_zip": "",
        "wireless_email": "",
        "systemnots": "00000",
        "unique_id": "10774659204d2b82e4e49d76.95645538",
        "profile_updated": null,
        "debit_card_type": "Amex",
        "androidurl": null,
        "flag_date": null,
        "accept_assignments": "0",
        "mbp_address2": "",
        "last_post_date": null,
        "mbp_city": "",
        "flikr_public": "0",
        "sms_address": "",
        "stringfly": "0",
        "thirdparty_assignments": "0",
        "cc_type": "Amex",
        "education_level": null,
        "cc_first_name": "",
        "mbp_phone": "",
        "about_me": null,
        "mbp_extno": "",
        "occupation": null,
        "state": "San Jose",
        "itunesurl": null,
        "activation_string": "",
        "mbp_email_address": "",
        "flag_comment": "",
        "register_date": "2011-01-10 17:06:28",
        "user_type": "mediabuyer",
        "cc_no": "",
        "current_longitude": "-84.045059",
        "flagged": "0",
        "fin_contact_check": "0",
        "mbp_first_name": "",
        "allow_gps_tracking": "0",
        "wireless_verified": "0",
        "company": "Sweneno",
        "user_permissions": "1-1-1-1-1-1-1",
        "link": "",
        "home_latitude": "9.926580",
        "anonymous": "0",
        "organization_type": "",
        "status": "1",
        "bburl": null,
        "allow_custom_recordings": "0",
        "numeric_ranking": null,
        "profile_id": "51",
        "last_visit_date": "2013-01-09 21:39:37",
        "payment_days": "0",
        "auth_type": null,
        "current_latitude": "9.926580",
        "allow_sms": "0",
        "cell_carrier": "0",
        "paypal_acc_no": "",
        "reason": "",
        "flag_review_date": null,
        "locale": "0",
        "user_specialty": "1",
        "create_assignments": "0",
        "last_sale_date": null,
        "ap_balance": "0.00",
        "gender": "Male",
        "cc_last_name": "",
        "verified": "0",
        "debit_card_no": "",
        "id": "1161",
        "cc_cvv": "",
        "birthday": "1900-01-01",
        "dcard_expdte": "2010-01-00",
        "picture_url": null,
        "master_user_id": "1161"
    }
	}

***

### Get Assignment Detail 
**End Point:** https://api.stringfly.com/v2/assignment/{assignment-id}  
**Request Method:** GET  
**Parameters:** None  
**Headers:** X-API-KEY, ACCESS-TOKEN  
  
**JSON Success:**
  
    {
    "success": 1,
    "content": [
        {
            "categories": "",
            "latitude": "9.939214999999992",
            "id": "761",
            "notify": "0",
            "showSmallBiw": "1",
            "priority": "0",
            "state": "",
            "relativeAlt": null,
            "criteria": "0",
            "embargo": "0",
            "channel_id": "1156",
            "content_treatment": "1",
            "geofencing_radio": "0.00",
            "review_ends_on": "0000-00-00 00:00:00",
            "custom_recording_thanks_url": "",
            "longitude": "-84.117317",
            "status": "1",
            "date_created": "0000-00-00 00:00:00",
            "description": "dfggdf",
            "alert_enabled": "1",
            "address": "",
            "review_count": "0",
            "custom_recording_url": "",
            "dimension": "1",
            "awards_per_video": null,
            "status_name": "0",
            "name": "dfgfg",
            "directions": "",
            "country": "",
            "distance": "0.0000000000",
            "showBiwOnClick": "1",
            "awards_currency": null,
            "users_id": "1264",
            "geofencing": "0",
            "awards_per_photo": null,
            "begins_on": "2011-07-08 10:35:00",
            "inFocus": "1",
            "purchase_count": "0",
            "alt": "1",
            "author_type": "0",
            "zip": "\"\"",
            "ends_on": "0000-00-00 00:00:00",
            "instructions": "",
            "doNotIndex": "1",
            "assign_type": "0",
            "call_back": "0",
            "city": "",
            "email_text": null,
            "keywords": "",
            "public": "0",
            "reporter_awards": "",
            "send_email": "0",
            "alert_type": "0",
            "featured": "0"
        }
    ]
	}

  
***

### List All Categories 
**End Point:** https://api.stringfly.com/v2/category  
**Request Method:** GET  
**Parameters:** None  
**Headers**: X-API-KEY, ACCESS-TOKEN  
  
**JSON Success:**

 	{
    "success": 1,
    "content": [
        {
            "color": "#66CCFF",
            "categories_id": "0",
            "category": "Accidents and Disasters",
            "master_category": null,
            "id": "5",
            "description": "Accidents and Disasters"
        },...
        {
            "color": "#FF6600",
            "categories_id": null,
            "category": "Arts and Entertainment",
            "master_category": null,
            "id": "1",
            "description": "Arts and Entertainment"
        }
    ]
  	}

***

### List All Carriers (not sure this is required any more) 
**End Point:** https://api.stringfly.com/v2/carrier  
**Request Method:** GET  
**Parameters:** None  
**Headers**: X-API-KEY, ACCESS-TOKEN  
  
**JSON Success:**

 	{
    "success": 1,
    "content": [
        {
            "sms_verified": "1",
            "sms_prefix": "",
            "sms_suffix": "@vtext.com",
            "name": "Verizon",
            "id": "2",
            "country_id": "78",
            "approved": "1",
            "description": "Verizon"
        },...
        {
            "sms_verified": "1",
            "sms_prefix": "",
            "sms_suffix": "@txt.att.net",
            "name": "ATT Wireless",
            "id": "3",
            "country_id": "78",
            "approved": "1",
            "description": "Gateway fails to handle complete phone numbers"
        }
    ]
  	}

***

### List All Assignments 
**End Point:** https://api.stringfly.com/v2/assignment  
**Request Method:** GET  
**Headers:** X-API-KEY, ACCESS-TOKEN  
**Parameters:**

>pagesize
>: the number of items to return for this call (optional, default: 10)
>
>page
>: the page number to return (for example, page 2 would return records 11 − 20) (optional, default: 1)

**JSON Response:**  

	{
    "success": 1,
    "content": [
        {
            "categories": "",
            "latitude": "25.728985",
            "id": "1622",
            "notify": "0",
            "showSmallBiw": "1",
            "priority": "0",
            "state": "Florida",
            "relativeAlt": null,
            "criteria": "0",
            "embargo": "0",
            "channel_id": "0",
            "content_treatment": "0",
            "geofencing_radio": "0.10",
            "review_ends_on": "0000-00-00 00:00:00",
            "custom_recording_thanks_url": "",
            "longitude": "-80.237419",
            "status": "4",
            "date_created": "2012-09-14 22:17:22",
            "description": "test",
            "alert_enabled": "0",
            "address": "",
            "review_count": "0",
            "custom_recording_url": "",
            "dimension": "1",
            "awards_per_video": "0",
            "status_name": "0",
            "name": "Fake",
            "directions": "",
            "country": "United States",
            "distance": "0.0000000000",
            "showBiwOnClick": "1",
            "awards_currency": "0",
            "users_id": "4667",
            "geofencing": "1",
            "awards_per_photo": "0",
            "begins_on": "2012-09-14 00:00:00",
            "inFocus": "1",
            "purchase_count": "0",
            "alt": "1",
            "author_type": "0",
            "zip": "\"\"",
            "ends_on": "2012-09-14 23:59:00",
            "instructions": "test",
            "doNotIndex": "1",
            "assign_type": "0",
            "call_back": "0",
            "city": "Miami",
            "email_text": "",
            "keywords": "",
            "public": "1",
            "reporter_awards": "",
            "send_email": "0",
            "alert_type": "0",
            "featured": "0"
        },...
        {
            "categories": "",
            "latitude": "25.728985",
            "id": "1621",
            "notify": "0",
            "showSmallBiw": "1",
            "priority": "0",
            "state": "Florida",
            "relativeAlt": null,
            "criteria": "0",
            "embargo": "0",
            "channel_id": "0",
            "content_treatment": "0",
            "geofencing_radio": "0.10",
            "review_ends_on": "0000-00-00 00:00:00",
            "custom_recording_thanks_url": "",
            "longitude": "-80.237419",
            "status": "4",
            "date_created": "2012-09-14 22:14:37",
            "description": "test",
            "alert_enabled": "0",
            "address": "",
            "review_count": "0",
            "custom_recording_url": "",
            "dimension": "1",
            "awards_per_video": "0",
            "status_name": "0",
            "name": "sample assignment",
            "directions": "",
            "country": "United States",
            "distance": "0.0000000000",
            "showBiwOnClick": "1",
            "awards_currency": "0",
            "users_id": "4667",
            "geofencing": "1",
            "awards_per_photo": "0",
            "begins_on": "2012-09-14 00:00:00",
            "inFocus": "1",
            "purchase_count": "0",
            "alt": "1",
            "author_type": "0",
            "zip": "\"\"",
            "ends_on": "2012-09-14 23:59:00",
            "instructions": "test",
            "doNotIndex": "1",
            "assign_type": "0",
            "call_back": "0",
            "city": "Miami",
            "email_text": "",
            "keywords": "",
            "public": "1",
            "reporter_awards": "",
            "send_email": "0",
            "alert_type": "0",
            "featured": "0"
        },
    ]
}

***

### List Assignment News Items  
**End Point:** https://api.stringfly.com/v2/assignment/{assignment-id}/newsitems  
**Request Method:** GET  
**Headers:** X-API-KEY, ACCESS-TOKEN  
**Parameters:**

>pagesize
>: the number of items to return for this call (optional, default: 10)
>
>page
>: the page number to return (for example, page 2 would return records 11 − 20) (optional, default: 1)

**JSON Success:**

	{
    	"success": 1,
    	"content": [
    	    {
    	        "title": "test-4",
    	        "id": "23867",
    	        "user_name": "anonuser"
    	    },...
    	    {
    	        "title": "test-4",
    	        "id": "23956",
    	        "user_name": "anonuser"
    	    }
    	]
	}

***

### List Category News Items  
**End Point:** https://api.stringfly.com/v2/category/{category-id}/newsitems  
**Request Method:** GET  
**Headers:** X-API-KEY, ACCESS-TOKEN  
**Parameters:**

>pagesize
>: the number of items to return for this call (optional, default: 10)
>
>page
>: the page number to return (for example, page 2 would return records 11 − 20) (optional, default: 1)

**JSON Success:**

	{
    	"success": 1,
    	"content": [
    	    {
    	        "title": "test-4",
    	        "id": "23867",
    	        "user_name": "anonuser"
    	    },...
    	    {
    	        "title": "test-4",
    	        "id": "23956",
    	        "user_name": "anonuser"
    	    }
    	]
	}

***

### Join Assignment   
**End Point:** https://api.stringfly.com/v2/assignment/{assignment-id}/join  
**Request Method:** POST  
**Headers:** X-API-KEY, ACCESS-TOKEN  
**Parameters:** None

**JSON Success:**

	{
    	"success": 1,
    	"content": [
    	    	]
	}
	
  
  
***

### List News Items 
**End Point:** https://api.stringfly.com/v2/newsitem  
**Request Method:** GET  
**Headers:** X-API-KEY, ACCESS-TOKEN  
**Parameters:**

>pagesize
>: the number of items to return for this call (optional, default: 10)
>
>page
>: the page number to return (for example, page 2 would return records 11 − 20) (optional, default: 1)

**JSON Success:**

	{
    	"success": 1,
    	"content": [
    	    {
    	        "title": "test-4",
    	        "id": "23867",
    	        "user_name": "anonuser"
    	    },...
    	    {
    	        "title": "test-4",
    	        "id": "23956",
    	        "user_name": "anonuser"
    	    }
    	]
	}

***

### Delete User  
**End Point:** https://api.stringfly.com/v2/user/{user-id} 
**Request Method:** DELETE  
**Headers:** X-API-KEY, ACCESS-TOKEN  
**Parameters:**

>email
>: the email address of the user you want to delete. It must belong to the user who’s id is in the endpoint (required)


**JSON Success:**

	{
    	"success": 1,
	    "content": {
        	"id": "4687",
        	"email": "ebetanxtest@gmail.com"
    	}
	}

***

### Create User 
**End Point:** https://api.stringfly.com/v2/user  
**Request Method:** POST  
**Headers:** X-API-KEY  
**Parameters:**

>email
>: the email address of the user you are creating (required)
>  
>username
>: the username for the user (required)
>  
>password
>: the password for the new user (required)
>  
>first_name
>: the user’s first (given) name (required)
>  
>last_name
>: tthe user’s last (familiy) name (required)
>  
> app_uid
> :  a unique identifier for this installation of the application (required)
> 
> app_description
> : a text description for this device or service (examples: "John's iPhone" and "My Cool Webservice") (required) 


**JSON Success:**

	{
    	"success": 1,
	    "content": {
        	"id": "4687",
        	"email": "ebetanxtest@gmail.com"
    	}
	}

***

### Upload File  
**End Point:** https://api.stringfly.com/v2/functions/files/upload  
**Request Method:** POST  
**Headers:** X-API-KEY,  ACCESS-TOKEN  
**Parameters:**

>files
>: the file being uploaded (required)

**Sample API Call from cURL:**  

 	$ curl -X POST -H "X-API-KEY: TESTTOKEN" -H 'ACCESS-TOKEN: NHwxZTIzMzkzZmU4Y2I2YjI2YzI5M2Q4NGVkMGExMWZkMA,,' -F   files=@yankring_history_v2.txt http://api.stringfly.dev/v2/functions/files/upload  

**JSON Response:**  

	{“success":1,"content":{"location":"developmedia.sweneno.com\/uploads\/TXTf1359785117ff590.TXT"}}  

***

###Assignment Search
**End Point:** https://api.stringfly.com/v2/assignment/search  
**Request Method:** GET  
**Headers:** X-API-KEY,  ACCESS-TOKEN  
**Parameters:**

>querytype
>:definition
>  
>page
>:definition
>  
>quantity
>:definition
>  
>queryactive
>:definition
>  
>queryexpired
>:definition
>  
>category
>:definition
>  
>latitude
>:definition
>  
>longitude
>:definition
>  
>geoname
>:definition
>  
>keyword
>:definition
>  
>startDate
>:definition
>  


**JSON Response:**  

	{
    "success": 1,
    "content": {
        "status": 0,
        "code": 104,
        "results": [
            {
                "public": "0",
                "ends_on": "03/09/12",
                "status": "4",
                "country": "",
                "ownerPicture": "http://developmedia.sweneno.com/profile_pics/large/9188667814f566b6f769171.93721166_1626744114.jpg",
                "latitude": "0",
                "name": "priv asignado",
                "users_id": "4574",
                "id": "1617",
                "longitude": "0",
                "begins_on": "03/06/12",
                "description": "sdkfjadslfkjdthumbnail thumbnail thumbnail thumbnail thumbnail sdkfjadslfkjdthumbnail thumbnail thumbnail thumbnail thumbnail sdkfjadslfkjdthumbnail thumbnail thumbnail thumbnail thumbnail sdkfjadslfkjdthumbnail thumbnail thumbnail thumbnail thumbnail sd"
            }
        ],
        "clientMessage": "",
        "serverMessage": "",
        "additionalData": "",
        "resultsCount": "1/4"
    }
	}

***

###Update User
**End Point:** https://api.stringfly.com/v2/user/{user-id}  
**Request Method:** POST  
**Headers:** X-API-KEY,  ACCESS-TOKEN  
**Parameters:**  

>user_name
>:
>
>password
>:
>
>email_address
>:
>
>first_name
>:
>
>last_name
>:
>
>salutation
>:
>
>address1
>:
>
>address2
>:
>
>city
>:
>
>state
>:
>
>zip
>:
>
>country
>:
>
>time_zone
>:
>
>birthday
>:
>
>gender
>:
>
>phone
>:
>
>language
>:
>
>cell_phone
>:
>
>about_me
>:
>
>work_history
>:
>
>education
>:
>
>agerange
>:
>
>occupation
>:
>
>suggestoccupation
>:
>
>educationlvl
>:
>
>suggesteducation
>:
>
>wireless_email
>:
>
>company
>:
>
>organization_type
>:
>
>locale
>:
>
>language
>:
>
>latitude
>:
>
>longitude
>:
>
>currency_symbol
>:
>
>allow_gps_tracking
>:
>
>allow_sms
>:
>
>accept_assignments
>:
>
>allow_contacts
>:
>
>allow_contests_nots
>:
>
>debit_card_no
>:
>
>debit_card_type
>:
>
>dcard_expdte
>:
>
>cc_no
>:
>
>cc_type
>:
>
>cc_exp_dte
>:
>
>paypal_acc_no
>:
>
>anonymous
>:
>

**JSON Response:**  

	{
	    "success": 1,
	    "content": {
    	    "id": "1161",
	        "data": {
            	"address1": "1500 NW 122 Ave."
        	}
    	}
	}
	
**NOTE:** the data element will contain all items that were updated by the api call and nothing more.

***

###Create News Item
**End Point:** https://api.stringfly.com/v2/newsitem  
**Request Method:** POST  
**Headers:** X-API-KEY,  ACCESS-TOKEN  
**Parameters:**  

>title
>:
>
>description
>:
>
>category
>:
>
>country
>:
>
>locality
>:
>
>longitude
>:
>
>latitude
>:
>
>altitude
>:
>
>called
>:
>
>asgid
>:The assignment Id if you are uploading the newsitem to a particular assignment
>
>filename
>:The name of the file returned by the image upload API call
>
>pack_id
>:
>
>pack
>:
>
>tempfilename
>:
>
>keywords
>:
>
>rotation
>:
>

**JSON Response:**  
	
	{
	    "success": 1,
	    "content": {
    	    "title": "Elliot's Sample TItle",
	        "filename": "developmedia.sweneno.com\\/uploads\\/TXTf1360280928149172d.TXT",
        	"description": "Elliot's Sample Description"
    	}
	}
	

**NOTE:** the data element will contain all items that were updated by the api call and nothing more.

***

###Update News Item
**End Point:** https://api.stringfly.com/v2/newsitem/{newsitem-id}  
**Request Method:** POST  
**Headers:** X-API-KEY,  ACCESS-TOKEN  
**Parameters: (note: all parameters are optional)**  

>title
>:
>
>description
>:
>
>category
>:
>
>country
>:
>
>locality
>:
>
>longitude
>:
>
>latitude
>:
>
>altitude
>:
>
>called
>:
>
>asgid
>:The assignment Id if you are uploading the newsitem to a particular assignment
>
>filename
>:The name of the file returned by the image upload API call
>
>pack_id
>:
>
>pack
>:
>
>tempfilename
>:
>
>keywords
>:
>
>rotation
>:
>

**JSON Response:**  

	{
    	"success": 1,
	    "content": {
        	"title": "panda"
    	}
	}
	
**NOTE:** the data element will contain all items that were updated by the api call and nothing more.

 ***
 
### Delete News Item  
**End Point:** https://api.stringfly.com/v2/newsitem/{newsitem-id}  
**Request Method:** DELETE  
**Headers:** X-API-KEY,  ACCESS-TOKEN  
**Parameters:**  

>none  

**JSON Response**

	{
    	"success": 1,
	    "content": {
        	"id": "23614"
    	}
	}

***

### Get User Statistics  
**End Point:** https://api.stringfly.com/v2/user/{user-id}/statistics  
**Request Method:** GET  
**Headers:** X-API-KEY,  ACCESS-TOKEN  
**Parameters:**  

>none  

**JSON Response**

	{
	    "success": 1,
	    "content": {
    	    "assignments_count": 23,
	        "uploads_count": 0,
    	    "views_count": 0
	    }
	}

***

### Probably Won’t Implement
7. List Users 

		